package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

type blurtPrice struct {
	PriceUSD float32 `json:"price_usd"`
	PriceBTC float32 `json:"price_btc"`
}

func getPrice(c *gin.Context) {

	data, err := ioutil.ReadFile("./blurt_price.json")
	if err != nil {
		fmt.Print(err)
	}

	var p blurtPrice

	err = json.Unmarshal(data, &p)
	if err != nil {
		fmt.Println("error:", err)
	}

	c.IndentedJSON(http.StatusOK, p)
}

func healthCheck(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, gin.H{"status": "OK"})
}

func main() {
	router := gin.Default()
	router.GET("/", healthCheck)
	router.GET("/price_info", getPrice)

	router.Run("localhost:4000")
}
